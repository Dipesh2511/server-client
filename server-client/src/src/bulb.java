package src;
import java.awt.Color;	
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class bulb extends JFrame {
	JLabel 				label;
	JTextField			textfield;
	JButton				button;
	
	
	public bulb(){
	setLayout(null);
	
	JLabel label = new JLabel("Input value");
	label.setBounds(100,100,100,20);
	this.add(label);
	
	
	JTextField textfield = new JTextField();
	textfield.setBounds(150,100,100,20);
	add(textfield);
	
	JButton button = new JButton("set value");
	button.setBounds(250,100,100,20);
	this.add(button);
	
	
	setTitle("bulb");
	setSize(500,500);
	setLocationRelativeTo(null);
	setDefaultCloseOperation(EXIT_ON_CLOSE);
	setVisible(true);
	
}


public String getBrightnessValue() {
	return textfield.getText();

	
}

private void setButtonActions() {
	
	this.button.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			int brightvalue = Integer.parseInt(textfield.getText());
			
			for(int i = 0; i > 0; i++) {
				button.setBackground(Color.white);
			}
			
			for(int i = 0; i < 100; i--) {
				button.setBackground(new Color(brightvalue).brighter());
			}
			
			
			
		}
	});
}

	public void paint(Graphics g){
		
		g.setColor(Color.black);
		g.fillOval(150, 200, 200, 200);	
		
	}
	
	
	public static void main(String[] args) {
		bulb b = new bulb();
		
	}



}